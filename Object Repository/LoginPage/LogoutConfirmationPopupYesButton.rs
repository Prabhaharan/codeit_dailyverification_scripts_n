<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>LogoutConfirmationPopupYesButton</name>
   <tag></tag>
   <elementGuidId>4f113238-5efb-46d5-94fa-25040be11c4f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//td[text()='Are you sure you want to logout?']/following::div[text()='Yes']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[text()='Are you sure you want to logout?']/following::div[text()='Yes']</value>
   </webElementProperties>
</WebElementEntity>
