<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PP_VerticalEllipsisIcon</name>
   <tag></tag>
   <elementGuidId>e3d3c7c5-b78a-405f-bb83-ed4033d457eb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//button[@class=&quot;md-icon-button mpl-item-menu-btn md-button md-ink-ripple&quot;] | //div[@data-v-6d59c8e3]/i[text()='more_vert'] | //div[@data-v-e5a0c77c]/i[text()='more_vert']
</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//button[@class=&quot;md-icon-button mpl-item-menu-btn md-button md-ink-ripple&quot;] | //div[@data-v-6d59c8e3]/i[text()='more_vert'] | //div[@data-v-e5a0c77c]/i[text()='more_vert']
</value>
   </webElementProperties>
</WebElementEntity>
