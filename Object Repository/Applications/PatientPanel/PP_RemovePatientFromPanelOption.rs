<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>PP_RemovePatientFromPanelOption</name>
   <tag></tag>
   <elementGuidId>c0f20528-4b50-4d04-9805-59708f8bbe73</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//div[contains(text(),'Remove from Panel')] | //span[text()='Remove from Panel']
</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[contains(text(),'Remove from Panel')] | //span[text()='Remove from Panel']
</value>
   </webElementProperties>
</WebElementEntity>
