<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CB_CBHP_SwitchToFrame</name>
   <tag></tag>
   <elementGuidId>f1c3519f-adf1-435c-bbc4-09129282b93f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@id = concat('//iframe[@id=' , &quot;'&quot; , 'carebook-iframe' , &quot;'&quot; , ']')]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//iframe[@id='carebook-iframe']</value>
   </webElementProperties>
</WebElementEntity>
