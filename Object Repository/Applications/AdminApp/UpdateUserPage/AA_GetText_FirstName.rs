<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AA_GetText_FirstName</name>
   <tag></tag>
   <elementGuidId>4d61f453-e141-442e-a75d-fb7453e79507</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'isc_TextItem_10']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()='First Name']//parent::nobr/..//following-sibling::td/input</value>
   </webElementProperties>
</WebElementEntity>
