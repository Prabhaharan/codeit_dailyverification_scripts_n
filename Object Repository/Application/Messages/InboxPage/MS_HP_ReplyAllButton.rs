<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MS_HP_ReplyAllButton</name>
   <tag></tag>
   <elementGuidId>919be8c8-8d9b-4aad-8bf7-81219cb7dbfb</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//tr/td/button[text()='Reply All']</value>
   </webElementProperties>
</WebElementEntity>
