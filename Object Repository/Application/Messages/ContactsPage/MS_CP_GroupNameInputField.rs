<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>MS_CP_GroupNameInputField</name>
   <tag></tag>
   <elementGuidId>9e460cb1-d5ec-4696-911b-da60951024c5</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//div[text()='New Group']//following::div//tbody//following::td/div//following::td/input[@type=&quot;text&quot;]</value>
   </webElementProperties>
</WebElementEntity>
