<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CareplanActivityStartDate</name>
   <tag></tag>
   <elementGuidId>b4fd208e-2816-4d17-9b42-3d8021366ec1</elementGuidId>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//*[text()='Careplan Activity Start Date']/following::input[3]</value>
   </webElementProperties>
</WebElementEntity>
