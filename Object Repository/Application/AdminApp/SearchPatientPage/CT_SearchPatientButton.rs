<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>CT_SearchPatientButton</name>
   <tag></tag>
   <elementGuidId>890e9d44-98fd-45dc-a62e-d6415fff2bbd</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//td[contains(text(),'Search Patient')]</value>
   </webElementProperties>
</WebElementEntity>
