<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>AA_RedAddress1</name>
   <tag></tag>
   <elementGuidId>d259294a-712a-496e-85fc-c3cbefca733e</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[contains(text(),'Address 1')]//parent::nobr/..//following-sibling::td//following::input[@name=&quot;isc_TextItem_13&quot;]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[contains(text(),'Address 1')]//parent::nobr/..//following-sibling::td//following::input[@name=&quot;isc_TextItem_13&quot;]</value>
   </webElementProperties>
</WebElementEntity>
