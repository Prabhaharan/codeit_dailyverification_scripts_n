<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_PW_ConsentPopupConsenterDropdownValue</name>
   <tag></tag>
   <elementGuidId>fdfa345d-9d49-4a47-ba7d-3d6c4ae0e1c1</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//md-option[contains(@ng-repeat,'consenterOptions ')][@aria-checked='true']/div</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//md-option[contains(@ng-repeat,'consenterOptions ')][@aria-checked='true']/div</value>
   </webElementProperties>
</WebElementEntity>
