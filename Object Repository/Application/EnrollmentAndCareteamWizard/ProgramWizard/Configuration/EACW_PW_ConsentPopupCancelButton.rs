<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_PW_ConsentPopupCancelButton</name>
   <tag></tag>
   <elementGuidId>bdcd3b96-a37b-4715-aeed-09dde79fa37d</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//p[contains(text(),&quot;Consent is required for this program&quot;)]//ancestor::md-dialog-content//following-sibling::md-dialog-actions/button/span[contains(text(),&quot;Cancel&quot;)]</value>
   </webElementProperties>
</WebElementEntity>
