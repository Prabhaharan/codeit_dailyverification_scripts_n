<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>EACW_UnassigningCareTeamPopupYesButton</name>
   <tag></tag>
   <elementGuidId>51af052b-43d2-47b2-a14f-d32685fd9d06</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//p[contains(text(),&quot;unassign the Care Team&quot;)]//ancestor::md-dialog/md-dialog-actions/button/span[text()=&quot;yes&quot;]</value>
   </webElementProperties>
</WebElementEntity>
