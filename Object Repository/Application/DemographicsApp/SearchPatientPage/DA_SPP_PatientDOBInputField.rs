<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DA_SPP_PatientDOBInputField</name>
   <tag></tag>
   <elementGuidId>c0cfca6d-095f-4f36-b38e-d0ac2f8393f8</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//*[@name = 'dateOfBirth_dateTextField']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>dateOfBirth_dateTextField</value>
   </webElementProperties>
</WebElementEntity>
