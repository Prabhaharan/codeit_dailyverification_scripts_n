<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>DA_VPP_PatientAddress1Field</name>
   <tag></tag>
   <elementGuidId>80a2cdb0-6655-4884-b3b2-a478e0019268</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>//label[text()='Address 1 ']/following::td[1]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>false</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>//label[text()='Address 1 ']/following::td[1]</value>
   </webElementProperties>
</WebElementEntity>
