
/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */

import java.lang.String

import com.kms.katalon.core.testobject.TestObject

import com.relevantcodes.extentreports.ExtentTest

import java.util.Map


def static "application.CarePlanApp.CarePlanAppNavigation"() {
    (new application.CarePlanApp()).CarePlanAppNavigation()
}

def static "application.CarePlanApp.PatientSearchFieldNavigation"() {
    (new application.CarePlanApp()).PatientSearchFieldNavigation()
}

def static "application.CarePlanApp.searchPatient"(
    	String patientID	
     , 	String patientLastName	) {
    (new application.CarePlanApp()).searchPatient(
        	patientID
         , 	patientLastName)
}

def static "application.CarePlanApp.searchPatientInCarePlanAppwithPatientID"(
    	String patientID	) {
    (new application.CarePlanApp()).searchPatientInCarePlanAppwithPatientID(
        	patientID)
}

def static "application.CarePlanApp.searchPatient"(
    	String patientID	) {
    (new application.CarePlanApp()).searchPatient(
        	patientID)
}

def static "runSelection.testcaseExecution.testcaseRun"(
    	String testcaseRunName	
     , 	String testSuiteRunName	) {
    (new runSelection.testcaseExecution()).testcaseRun(
        	testcaseRunName
         , 	testSuiteRunName)
}

def static "populationManager.SOPVerification.alertReportPageVerification"() {
    (new populationManager.SOPVerification()).alertReportPageVerification()
}

def static "populationManager.SOPVerification.assessmentReportPageVerification"() {
    (new populationManager.SOPVerification()).assessmentReportPageVerification()
}

def static "populationManager.SOPVerification.btqDemographicReportPageVerification"() {
    (new populationManager.SOPVerification()).btqDemographicReportPageVerification()
}

def static "populationManager.SOPVerification.btqEncounterActivityReportPageVerification"() {
    (new populationManager.SOPVerification()).btqEncounterActivityReportPageVerification()
}

def static "populationManager.SOPVerification.carePlanEncounterReportPageVerification"() {
    (new populationManager.SOPVerification()).carePlanEncounterReportPageVerification()
}

def static "populationManager.SOPVerification.carePlanGapReportPageVerification"() {
    (new populationManager.SOPVerification()).carePlanGapReportPageVerification()
}

def static "populationManager.SOPVerification.careTeamReportPageVerification"() {
    (new populationManager.SOPVerification()).careTeamReportPageVerification()
}

def static "populationManager.SOPVerification.healthixAlertsReportPageVerification"() {
    (new populationManager.SOPVerification()).healthixAlertsReportPageVerification()
}

def static "populationManager.SOPVerification.issuesReportPageVerification"() {
    (new populationManager.SOPVerification()).issuesReportPageVerification()
}

def static "populationManager.SOPVerification.patientProgrammingInformationReportPageVerification"() {
    (new populationManager.SOPVerification()).patientProgrammingInformationReportPageVerification()
}

def static "populationManager.SOPVerification.patientEnrollmentReportPageVerification"() {
    (new populationManager.SOPVerification()).patientEnrollmentReportPageVerification()
}

def static "application.PatientPanel.searchPatientInThePatientPanel"(
    	String patientId	
     , 	String patientLastName	) {
    (new application.PatientPanel()).searchPatientInThePatientPanel(
        	patientId
         , 	patientLastName)
}

def static "populationManager.InteractiveReport.ScrollUsingJs"() {
    (new populationManager.InteractiveReport()).ScrollUsingJs()
}

def static "populationManager.InteractiveReport.DoubleClickUsingJS"(
    	TestObject to	
     , 	int timeout	) {
    (new populationManager.InteractiveReport()).DoubleClickUsingJS(
        	to
         , 	timeout)
}

def static "populationManager.InteractiveReport.dispatchEventClickUsingJS"(
    	TestObject to	
     , 	int timeout	) {
    (new populationManager.InteractiveReport()).dispatchEventClickUsingJS(
        	to
         , 	timeout)
}

def static "populationManager.InteractiveReport.clickUsingJS"(
    	TestObject to	
     , 	int timeout	) {
    (new populationManager.InteractiveReport()).clickUsingJS(
        	to
         , 	timeout)
}

def static "populationManager.InteractiveReport.keypressUsingJS"() {
    (new populationManager.InteractiveReport()).keypressUsingJS()
}

def static "populationManager.InteractiveReport.CreateFile"() {
    (new populationManager.InteractiveReport()).CreateFile()
}

def static "populationManager.InteractiveReport.PressUsingJs"() {
    (new populationManager.InteractiveReport()).PressUsingJs()
}

def static "populationManager.InteractiveReport.dragAndDrop"(
    	TestObject sourceObject	
     , 	TestObject destinationObject	) {
    (new populationManager.InteractiveReport()).dragAndDrop(
        	sourceObject
         , 	destinationObject)
}

def static "populationManager.InteractiveReport.isNumericValue"(
    	String str	) {
    (new populationManager.InteractiveReport()).isNumericValue(
        	str)
}

def static "commonutils.commonkeyword.randomTextalone"() {
    (new commonutils.commonkeyword()).randomTextalone()
}

def static "application.AlertApp.alertappnavigate"() {
    (new application.AlertApp()).alertappnavigate()
}

def static "application.AlertApp.dropdownone"() {
    (new application.AlertApp()).dropdownone()
}

def static "application.AlertApp.dropdowntwo"() {
    (new application.AlertApp()).dropdowntwo()
}

def static "keywordsLibrary.CommomLibrary.clickUsingJS"(
    	TestObject to	
     , 	int timeout	) {
    (new keywordsLibrary.CommomLibrary()).clickUsingJS(
        	to
         , 	timeout)
}

def static "keywordsLibrary.CommomLibrary.randomText"() {
    (new keywordsLibrary.CommomLibrary()).randomText()
}

def static "keywordsLibrary.CommomLibrary.addGlobalVariable"(
    	String name	
     , 	Object value	) {
    (new keywordsLibrary.CommomLibrary()).addGlobalVariable(
        	name
         , 	value)
}

def static "keywordsLibrary.CommomLibrary.getRandomNumber"() {
    (new keywordsLibrary.CommomLibrary()).getRandomNumber()
}

def static "keywordsLibrary.CommomLibrary.getRandomNumberuptofivedigit"() {
    (new keywordsLibrary.CommomLibrary()).getRandomNumberuptofivedigit()
}

def static "keywordsLibrary.CommomLibrary.getRandomNumberuptotendigit"() {
    (new keywordsLibrary.CommomLibrary()).getRandomNumberuptotendigit()
}

def static "keywordsLibrary.CommomLibrary.dynamicElement"(
    	String xpath	) {
    (new keywordsLibrary.CommomLibrary()).dynamicElement(
        	xpath)
}

def static "keywordsLibrary.CommomLibrary.dynamicElements"(
    	String xpath	) {
    (new keywordsLibrary.CommomLibrary()).dynamicElements(
        	xpath)
}

def static "keywordsLibrary.CommomLibrary.randomSingleText"() {
    (new keywordsLibrary.CommomLibrary()).randomSingleText()
}

def static "keywordsLibrary.CommomLibrary.carePlanElementClick"(
    	TestObject To	) {
    (new keywordsLibrary.CommomLibrary()).carePlanElementClick(
        	To)
}

def static "keywordsLibrary.CommomLibrary.clickMouseOver"(
    	TestObject to	) {
    (new keywordsLibrary.CommomLibrary()).clickMouseOver(
        	to)
}

def static "keywordsLibrary.CommomLibrary.randomTextalone"() {
    (new keywordsLibrary.CommomLibrary()).randomTextalone()
}

def static "keywordsLibrary.CommomLibrary.randomTextaloneEmail"() {
    (new keywordsLibrary.CommomLibrary()).randomTextaloneEmail()
}

def static "populationManager.SOP.alertReportPageNavigation"() {
    (new populationManager.SOP()).alertReportPageNavigation()
}

def static "populationManager.SOP.assessmentReportPageNavigation"() {
    (new populationManager.SOP()).assessmentReportPageNavigation()
}

def static "populationManager.SOP.bTQDemographicReportPageNavigation"() {
    (new populationManager.SOP()).bTQDemographicReportPageNavigation()
}

def static "populationManager.SOP.bTQEncounterActivityReportPageNavigation"() {
    (new populationManager.SOP()).bTQEncounterActivityReportPageNavigation()
}

def static "populationManager.SOP.CarePlanEncounterActivityReportPageNavigation"() {
    (new populationManager.SOP()).CarePlanEncounterActivityReportPageNavigation()
}

def static "populationManager.SOP.CarePlanGapReportPageNavigation"() {
    (new populationManager.SOP()).CarePlanGapReportPageNavigation()
}

def static "populationManager.SOP.CareTeamReportPageNavigation"() {
    (new populationManager.SOP()).CareTeamReportPageNavigation()
}

def static "populationManager.SOP.HealthixAlertsReportPageNavigation"() {
    (new populationManager.SOP()).HealthixAlertsReportPageNavigation()
}

def static "populationManager.SOP.IssuesReportPageNavigation"() {
    (new populationManager.SOP()).IssuesReportPageNavigation()
}

def static "populationManager.SOP.PatientProgramInformationReportPageNavigation"() {
    (new populationManager.SOP()).PatientProgramInformationReportPageNavigation()
}

def static "populationManager.SOP.ProgramEnrollmentReportPageNavigation"() {
    (new populationManager.SOP()).ProgramEnrollmentReportPageNavigation()
}

def static "keywordsLibrary.newkeyword.ScrollUsingJs"() {
    (new keywordsLibrary.newkeyword()).ScrollUsingJs()
}

def static "keywordsLibrary.newkeyword.DoubleClickUsingJS"(
    	TestObject to	
     , 	int timeout	) {
    (new keywordsLibrary.newkeyword()).DoubleClickUsingJS(
        	to
         , 	timeout)
}

def static "keywordsLibrary.newkeyword.dispatchEventClickUsingJS"(
    	TestObject to	
     , 	int timeout	) {
    (new keywordsLibrary.newkeyword()).dispatchEventClickUsingJS(
        	to
         , 	timeout)
}

def static "keywordsLibrary.newkeyword.clickUsingJS"(
    	TestObject to	
     , 	int timeout	) {
    (new keywordsLibrary.newkeyword()).clickUsingJS(
        	to
         , 	timeout)
}

def static "keywordsLibrary.newkeyword.keypressUsingJS"() {
    (new keywordsLibrary.newkeyword()).keypressUsingJS()
}

def static "keywordsLibrary.newkeyword.CreateFile"() {
    (new keywordsLibrary.newkeyword()).CreateFile()
}

def static "keywordsLibrary.newkeyword.PressUsingJs"() {
    (new keywordsLibrary.newkeyword()).PressUsingJs()
}

def static "keywordsLibrary.newkeyword.dragAndDrop"(
    	TestObject sourceObject	
     , 	TestObject destinationObject	) {
    (new keywordsLibrary.newkeyword()).dragAndDrop(
        	sourceObject
         , 	destinationObject)
}

def static "keywordsLibrary.newkeyword.isNumericValue"(
    	String str	) {
    (new keywordsLibrary.newkeyword()).isNumericValue(
        	str)
}

def static "application.TaskManagerApp.navigateToTaskManager"() {
    (new application.TaskManagerApp()).navigateToTaskManager()
}

def static "application.CareBookApp.careBookSearchField"(
    	String patientFirstName	
     , 	String patientLastName	) {
    (new application.CareBookApp()).careBookSearchField(
        	patientFirstName
         , 	patientLastName)
}

def static "keywordsLibrary.randomdate.getRandomDate"() {
    (new keywordsLibrary.randomdate()).getRandomDate()
}

def static "keywordsLibrary.randomdate.getMajorDateInput"() {
    (new keywordsLibrary.randomdate()).getMajorDateInput()
}

def static "keywordsLibrary.randomdate.getMinorDateInput"() {
    (new keywordsLibrary.randomdate()).getMinorDateInput()
}

def static "keywordsLibrary.randomdate.getDateInput"() {
    (new keywordsLibrary.randomdate()).getDateInput()
}

def static "reports.extentReports.getInstance"() {
    (new reports.extentReports()).getInstance()
}

def static "reports.extentReports.print"(
    	ExtentTest test	
     , 	boolean verifyStatus	
     , 	String printStatement	) {
    (new reports.extentReports()).print(
        	test
         , 	verifyStatus
         , 	printStatement)
}

def static "reports.extentReports.takeScreenshot"(
    	ExtentTest test	) {
    (new reports.extentReports()).takeScreenshot(
        	test)
}

def static "keywordsLibrary.careTeamAssignandRemove.navigateToCareTeamWizardPage"() {
    (new keywordsLibrary.careTeamAssignandRemove()).navigateToCareTeamWizardPage()
}

def static "keywordsLibrary.careTeamAssignandRemove.savingtheCareTeam"() {
    (new keywordsLibrary.careTeamAssignandRemove()).savingtheCareTeam()
}

def static "keywordsLibrary.careTeamAssignandRemove.savingtheAdminApp"() {
    (new keywordsLibrary.careTeamAssignandRemove()).savingtheAdminApp()
}

def static "keywordsLibrary.careTeamAssignandRemove.clickEnrollmentButton"() {
    (new keywordsLibrary.careTeamAssignandRemove()).clickEnrollmentButton()
}

def static "keywordsLibrary.careTeamAssignandRemove.closingtheCareTeamWizard"() {
    (new keywordsLibrary.careTeamAssignandRemove()).closingtheCareTeamWizard()
}

def static "keywordsLibrary.careTeamAssignandRemove.verifyAssignedCareTeam"() {
    (new keywordsLibrary.careTeamAssignandRemove()).verifyAssignedCareTeam()
}

def static "keywordsLibrary.careTeamAssignandRemove.removingAssignedCareTeam"() {
    (new keywordsLibrary.careTeamAssignandRemove()).removingAssignedCareTeam()
}

def static "keywordsLibrary.careTeamAssignandRemove.unassignPopUpandClosingWizard"() {
    (new keywordsLibrary.careTeamAssignandRemove()).unassignPopUpandClosingWizard()
}

def static "populationManager.InteractivePatientsVerification.ScrollUsingJs"() {
    (new populationManager.InteractivePatientsVerification()).ScrollUsingJs()
}

def static "populationManager.InteractivePatientsVerification.DoubleClickUsingJS"(
    	TestObject to	
     , 	int timeout	) {
    (new populationManager.InteractivePatientsVerification()).DoubleClickUsingJS(
        	to
         , 	timeout)
}

def static "populationManager.InteractivePatientsVerification.RightClickUsingJS"(
    	TestObject to	
     , 	int timeout	) {
    (new populationManager.InteractivePatientsVerification()).RightClickUsingJS(
        	to
         , 	timeout)
}

def static "populationManager.InteractivePatientsVerification.dispatchEventClickUsingJS"(
    	TestObject to	
     , 	int timeout	) {
    (new populationManager.InteractivePatientsVerification()).dispatchEventClickUsingJS(
        	to
         , 	timeout)
}

def static "populationManager.InteractivePatientsVerification.clickUsingJS"(
    	TestObject to	
     , 	int timeout	) {
    (new populationManager.InteractivePatientsVerification()).clickUsingJS(
        	to
         , 	timeout)
}

def static "populationManager.InteractivePatientsVerification.keypressUsingJS"() {
    (new populationManager.InteractivePatientsVerification()).keypressUsingJS()
}

def static "populationManager.InteractivePatientsVerification.CreateFile"() {
    (new populationManager.InteractivePatientsVerification()).CreateFile()
}

def static "populationManager.InteractivePatientsVerification.PressUsingJs"() {
    (new populationManager.InteractivePatientsVerification()).PressUsingJs()
}

def static "populationManager.InteractivePatientsVerification.dragAndDrop"(
    	TestObject sourceObject	
     , 	TestObject destinationObject	) {
    (new populationManager.InteractivePatientsVerification()).dragAndDrop(
        	sourceObject
         , 	destinationObject)
}

def static "populationManager.InteractivePatientsVerification.isNumericValue"(
    	String str	) {
    (new populationManager.InteractivePatientsVerification()).isNumericValue(
        	str)
}

def static "application.CareBookUniversalSearch.careBookUniversalSearchInputField"(
    	String patientFirstName	
     , 	String patientLastName	) {
    (new application.CareBookUniversalSearch()).careBookUniversalSearchInputField(
        	patientFirstName
         , 	patientLastName)
}

def static "populationManager.DragandDrop.dragAndDrop"(
    	TestObject sourceObject	
     , 	TestObject destinationObject	) {
    (new populationManager.DragandDrop()).dragAndDrop(
        	sourceObject
         , 	destinationObject)
}

def static "application.DemographicsApp.DemographcisAppNavigation"() {
    (new application.DemographicsApp()).DemographcisAppNavigation()
}

def static "application.DemographicsApp.searchPatientInDemographcisAppwithLN"(
    	String patientID	
     , 	String patientLastName	) {
    (new application.DemographicsApp()).searchPatientInDemographcisAppwithLN(
        	patientID
         , 	patientLastName)
}

def static "application.DemographicsApp.searchPatientInDemographcisAppwithPatientID"(
    	String patientID	) {
    (new application.DemographicsApp()).searchPatientInDemographcisAppwithPatientID(
        	patientID)
}

def static "application.DemographicsApp.searchPatientInDemographcisAppwithPatientIDOne"(
    	String patientID1	) {
    (new application.DemographicsApp()).searchPatientInDemographcisAppwithPatientIDOne(
        	patientID1)
}

def static "application.DemographicsApp.searchPatientInDemographcisAppwithFN"(
    	String patientID	
     , 	String firstName	) {
    (new application.DemographicsApp()).searchPatientInDemographcisAppwithFN(
        	patientID
         , 	firstName)
}

def static "application.DemographicsApp.searchPatientInDemographcisApp"(
    	String patientID	
     , 	String patientLastName	) {
    (new application.DemographicsApp()).searchPatientInDemographcisApp(
        	patientID
         , 	patientLastName)
}

def static "application.DemographicsApp.selectPatientInDemographcisApp"() {
    (new application.DemographicsApp()).selectPatientInDemographcisApp()
}

def static "application.DemographicsApp.searchPatientInDemographcisApp"(
    	String patientLastName	) {
    (new application.DemographicsApp()).searchPatientInDemographcisApp(
        	patientLastName)
}

def static "application.DemographicsApp.searchPatientInDemographcisAppwithdifferentField"(
    	java.util.Map<String, String> verifyfields	) {
    (new application.DemographicsApp()).searchPatientInDemographcisAppwithdifferentField(
        	verifyfields)
}

def static "application.PreferncesKeyword.PrefernecesNavigation"() {
    (new application.PreferncesKeyword()).PrefernecesNavigation()
}

def static "application.PreferncesKeyword.PreferenceLink"() {
    (new application.PreferncesKeyword()).PreferenceLink()
}

def static "application.CareTeamWizard.navigateCareTeamWizard"(
    	String patientID	
     , 	String patientLastName	) {
    (new application.CareTeamWizard()).navigateCareTeamWizard(
        	patientID
         , 	patientLastName)
}

def static "application.CareTeamWizard.removeMember"() {
    (new application.CareTeamWizard()).removeMember()
}

def static "application.CareTeamWizard.addMember"(
    	String CareTeamAdd	) {
    (new application.CareTeamWizard()).addMember(
        	CareTeamAdd)
}

def static "application.CareTeamWizard.navigateAddNewMemberPage"() {
    (new application.CareTeamWizard()).navigateAddNewMemberPage()
}

def static "application.CareTeamWizard.careteamassign"(
    	String ExistingCareTeamNameSelect	) {
    (new application.CareTeamWizard()).careteamassign(
        	ExistingCareTeamNameSelect)
}

def static "application.AdminApp.AdminAppNavigation"() {
    (new application.AdminApp()).AdminAppNavigation()
}

def static "application.AdminApp.searchUserInAdminAppwithlastName"(
    	String userLastName	) {
    (new application.AdminApp()).searchUserInAdminAppwithlastName(
        	userLastName)
}

def static "application.AdminApp.searchPatientInAdminAppwithdifferentField"(
    	java.util.Map<String, String> verifyfields	) {
    (new application.AdminApp()).searchPatientInAdminAppwithdifferentField(
        	verifyfields)
}

def static "application.PopulationManagerIcon.PopulationManagerAppNavigation"() {
    (new application.PopulationManagerIcon()).PopulationManagerAppNavigation()
}

def static "application.PopulationManagerIcon.StandardOperationReportNavigation"() {
    (new application.PopulationManagerIcon()).StandardOperationReportNavigation()
}

def static "application.PopulationManagerIcon.isNumericValue"(
    	String str	) {
    (new application.PopulationManagerIcon()).isNumericValue(
        	str)
}

def static "application.LoginLogout.Login"(
    	String url	
     , 	String userName	
     , 	String password	) {
    (new application.LoginLogout()).Login(
        	url
         , 	userName
         , 	password)
}

def static "application.LoginLogout.Logout"() {
    (new application.LoginLogout()).Logout()
}

def static "application.LoginLogout.CarePlanLogout"() {
    (new application.LoginLogout()).CarePlanLogout()
}

def static "application.MessagesApp.MessagesAppNavigation"() {
    (new application.MessagesApp()).MessagesAppNavigation()
}

def static "application.MessagesApp.VerifyInboxEmail"(
    	String mailSubject	) {
    (new application.MessagesApp()).VerifyInboxEmail(
        	mailSubject)
}

def static "application.MessagesApp.SearchEmail"(
    	String textValue	) {
    (new application.MessagesApp()).SearchEmail(
        	textValue)
}

def static "application.CareTeamApp.careTeamAppNavigation"() {
    (new application.CareTeamApp()).careTeamAppNavigation()
}

def static "application.CareTeamApp.fieldSearch"(
    	String fieldValue	
     , 	java.util.Map<String, String> inputValue	) {
    (new application.CareTeamApp()).fieldSearch(
        	fieldValue
         , 	inputValue)
}

def static "application.CareTeamApp.searchPatientInCareTeamAppwithPatientID"(
    	String patientID	) {
    (new application.CareTeamApp()).searchPatientInCareTeamAppwithPatientID(
        	patientID)
}

def static "application.CareTeamApp.selectPatientInCareTeamApp"() {
    (new application.CareTeamApp()).selectPatientInCareTeamApp()
}

def static "application.CareTeamApp.searchPatientInCareTeamAppwithdifferentField"(
    	java.util.Map<String, String> verifyfields	) {
    (new application.CareTeamApp()).searchPatientInCareTeamAppwithdifferentField(
        	verifyfields)
}

def static "application.ProgramWizard.deleteProgram"(
    	int existingProgramsCount	) {
    (new application.ProgramWizard()).deleteProgram(
        	existingProgramsCount)
}
